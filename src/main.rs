use failure::Fallible;

#[macro_use]
extern crate unhtml_derive;
use unhtml::FromHtml;

#[derive(Debug, FromHtml)]
#[html(selector = ".time-articles")]
struct Articles {
    #[html(selector = ".article a", attr="href")]
    articles: Vec<String>,
}

#[derive(Debug, FromHtml)]
#[html(selector = ".post-item")]
struct Article {
    #[html(selector = "img", attr="src")]
    images: Vec<String>,

    #[html(selector = "h1, h2, p", attr="inner")]
    text: Vec<String>,
}

#[tokio::main]
async fn main() -> Fallible<()> {
    let main = reqwest::get("https://korrespondent.net")
        .await?
        .text()
        .await?;

    let articles = Articles::from_html(&main)?
        .articles
        .into_iter()
        .take(20)
        .map(|url| async move {
            let article = scrape_article(&url).await?;
            println!("{}: {} text fragments", url, article.text.len());
            Ok((url, article))
        });
    let articles = futures::future::join_all(articles).await
        .into_iter()
        .collect::<Fallible<Vec<_>>>()?;

    let xml = make_xml(&articles);
    tokio::fs::write("result.xml", xml).await?;

    Ok(())
}

async fn scrape_article(url: &str) -> Fallible<Article> {
    let page = reqwest::get(url)
        .await?
        .text()
        .await?;

    Ok(Article::from_html(&page)?)
}

fn make_xml(articles: &[(String, Article)]) -> String {
    let mut writer = xmlwriter::XmlWriter::new(Default::default());
    writer.start_element("data");
    for (url, article) in articles {
        writer.start_element("article");
        writer.write_attribute("url", &url);
        for image in &article.images {
            writer.start_element("fragment");
            writer.write_attribute("type", "image");
            writer.write_text(image);
            writer.end_element();
        }
        for text in &article.text {
            writer.start_element("fragment");
            writer.write_attribute("type", "text");
            writer.write_text(text);
            writer.end_element();
        }
        writer.end_element();
    }
    writer.end_element();
    writer.end_document()
}
